:sequential_nav: next

..  _tutorial-basic-basis-sets:

Basis sets
==========

..  sidebar:: **Have you set up the local environment?**

    If not, :ref:`do that now <local_installation>` before proceeding.

In this exercise we will look more closely at the basis sets used in
Siesta.

.. note::
   The material from the old `BasisSets-XXXX` tutorials is geared
   towards exploration of convergence issues. A previous section is
   needed to show the students the ways in which basis info is
   entered, how to check what the program has done, maybe look at the
   orbitals, etc.


The basis set is a key ingredient in any Siesta calculation. The
program can construct a default basis set, but this would in general
not be appropriate for production calculations.  [Link to appropriate
section of the manual, and to the background section for philosophy]

Siesta offers may options for the specification of the basis set. We
will look first at two key concepts to explore: cardinality of the set
and range of the orbitals::

  PAO.basis-size SZP
  PAO.energy-shift 0.01 Ry

[Run an example and look at the basis-set section in the
output. Highlight here...].

Mention also the .ion files, and how to plot the information in them.
[Note that ionplot is not currently in the VM]. Sisl can also be used
for this (using the .ion.xml files). This will go in a :ref:`HOW-TO <how-to-orbital-visualization>`.

(Change the options in the block above and explore)

Basis sets for molecules
------------------------

(This can reuse the legacy tutorial material)

Basis sets for crystals
------------------------

The legacy material is a bit hard, and the choice of system strange.





   

   

:sequential_nav: next

..  _tutorial-basic-scf-convergence:

The self-consistent-field cycle
===============================

..  sidebar:: **Have you set up the local environment?**

    If not, :ref:`do that now <local_installation>` before proceeding.

In this exercise we will look more closely at the scf cycle
and how to monitor its convergence

.. warning::
   Rest of file under construction

Figures:  Basic sketch of the scf cycle (good one in Roberto's BSC
2017 convergence talk).

.. note::
   focus on: mixing techniques and appropriate tolerances; how to
   specify the options in the fdf file (complex). How to restart (link
   to module on output files).
   Systems to treat: we need at least two. Examples?

   


  
  
   

   

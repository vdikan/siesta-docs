:sequential_nav: next

..  _tutorial-advanced-analysis:

Advanced analysis of the electronic structure
=============================================

.. note::
   This will contain the COOP/COHP bonding analysis, the fat-bands, and
   the spin texture processing. All three share a basis of theoretical
   concepts (use of wavefunctions as starting point), and actually
   reside in the same place (Util/COOP) in the distribution.

.. toctree::
    :maxdepth: 1
    :numbered:

    coop-cohp/index
    fatbands/index
    spin-texture/index

    


